/**
 * Created by Alexey on 10-Oct-15.
 */


//      < GLOBAL VARIABLES >
var SCREEN_WIDTH = 1280, SCREEN_HEIGHT = 720,
    START_TIME = Date.now();
//      </GLOBAL VARIABLES>



if(!Detector.webgl){
    Detector.addGetWebGLMessage();
}

// -------INIT STATS
var stats = new Stats();
stats.setMode( 0 ); // 0: fps, 1: ms, 2: mb
stats.domElement.style.position = 'absolute';
stats.domElement.style.left = '0px';
stats.domElement.style.top = '0px';
document.body.appendChild( stats.domElement );
// </INIT STATS>

var resourcesConfig = {
    "demo.glsl" : { url : "shaders/demo.glsl"}
};

Resources.loadResources(resourcesConfig, function(){
    var renderer = PIXI.autoDetectRenderer(SCREEN_WIDTH, SCREEN_HEIGHT);
    document.body.appendChild(renderer.view);

    MouseHandler.initListeners( renderer.view );

// create the root of the scene graph
    var stage = new PIXI.Container();

// create a texture from an image path
    var romaSunTex = PIXI.Texture.fromImage('images/roma_sun256.png');
    var romaBulbTex = PIXI.Texture.fromImage('images/roma2.png');

// create a new Sprite using the texture
    var renderSprite = new PIXI.Sprite();
    renderSprite.width = SCREEN_WIDTH;
    renderSprite.height = SCREEN_HEIGHT;
    renderSprite.x = 0;
    renderSprite.y = 0;


    var uniforms = {
        "time" : { type : "1f", value : 0},
        "resolution" : { type : "v2", value : { x: SCREEN_WIDTH, y: SCREEN_HEIGHT } },
        "customTex1" : { type : "sampler2D", value : romaBulbTex },
        "customTex2" : { type : "sampler2D", value : romaSunTex },
        "mousePos" : { type : "v2", value : { x: 10, y: 10 } }
    };

    var filter = new PIXI.AbstractFilter( null, Resources.getResource("demo.glsl"), uniforms );

    stage.filters = [filter];

    stage.addChild(renderSprite);

    function onResize( e ){
        SCREEN_WIDTH = window.innerWidth;
        SCREEN_HEIGHT = window.innerHeight;

        renderSprite.width = SCREEN_WIDTH;
        renderSprite.height = SCREEN_HEIGHT;

        uniforms.resolution.value.x = SCREEN_WIDTH;
        uniforms.resolution.value.y = SCREEN_HEIGHT;


        renderer.resize(SCREEN_WIDTH, SCREEN_HEIGHT);
    }

    window.addEventListener("resize", onResize);

    onResize();

// start animating
    animate();
    var mp;
    function animate() {
        requestAnimationFrame(animate);

        uniforms.time.value = (Date.now() - START_TIME) * 0.01;
        mp = MouseHandler.getPosition();
        uniforms.mousePos.value.x = mp[0];
        uniforms.mousePos.value.y = mp[1];
        stats.begin();
        // render the container
        renderer.render( stage );

        stats.end();
    }
});
